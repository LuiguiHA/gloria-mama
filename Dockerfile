FROM php:5.6-apache
RUN a2enmod rewrite
RUN docker-php-ext-install pdo pdo_mysql mbstring json 
COPY php.ini /usr/local/etc/php/
RUN usermod -u 1000 www-data
RUN mkdir -p /tmp/php
RUN chmod -R 777 /tmp
RUN apt-get update && apt-get install --yes --fix-missing \
        zip \
        vim \
        curl \
        build-essential\ 
        git \
        sox \
        libsox-fmt-mp3 \
        locales \
       libnotify-bin \
       libfreetype6-dev \
       libjpeg62-turbo-dev \
       libmcrypt-dev \
       libpng12-dev \
   && docker-php-ext-install -j$(nproc) iconv mcrypt \
   && docker-php-ext-configure gd --with-freetype-dir=/usr/include/ --with-jpeg-dir=/usr/include/ \
   && docker-php-ext-install -j$(nproc) gd
RUN export LANGUAGE=en_US.UTF-8; export LANG=en_US.UTF-8; export LC_ALL=en_US.UTF-8; locale-gen en_US.UTF-8; DEBIAN_FRONTEND=noninteractive dpkg-reconfigure locales
RUN locale-gen es_ES.UTF-8; DEBIAN_FRONTEND=noninteractive dpkg-reconfigure locales
RUN php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
RUN php -r "if (hash_file('SHA384', 'composer-setup.php') === '544e09ee996cdf60ece3804abc52599c22b1f40f4323403c44d44fdfdd586475ca9813a858088ffbc1f233e9b180f061') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;"
RUN php composer-setup.php
RUN php -r "unlink('composer-setup.php');"
RUN COMPOSER_CACHE_DIR=/var/www/html
RUN COMPOSER_ALLOW_SUPERUSER=1
CMD ["apache2-foreground"]
