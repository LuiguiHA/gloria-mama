Vue.use(VeeValidate,{fieldsBagName:'fieldsVee',errorBagName:'errorsVee', events:"input|change"})
VeeValidate.Validator.setLocale("es")
var BeatLoader = VueSpinner.BeatLoader
var app = new Vue({
  el: '#app',
  delimiters: ['${', '}'],
  components: {
    BeatLoader
  },
  data: {
    name: '',
    alert: false,
    url: '',
    email: '',
    loadingEmail: false,
    loading: false
  },
  watch: {
  },
  mounted () {
    document.getElementById('app').style.display = "block";
    // if (window.url == undefined){
      // setTimeout(function(){
      // //   var vid = document.getElementById("video"); 
      // //   vid.play();
      //   $("video")[0].play();
      // }.bind(this), 1000)

      // var video = document.createElement('video');

      // video.src = window.url;
      // video.autoplay = true;
      
      // $("#video-container").append(video)
    // }

    $('#video-container video').bind('play', function (e) {
        $(".play-btn").hide();
    });

    $('#video-container video').bind('ended', function (e) {
        $(".play-btn").show(); 
    });
    

    var userAgent = window.navigator.userAgent;

    if (userAgent.match(/iPad/i) || userAgent.match(/iPhone/i)) {
      $("video")[0].controls = true
    }

  },
  methods:{
    playVideo: function(){
      $("video")[0].play()
    },
    sendVideo: function(index){
      this.$validator.validateAll().then((result) => {
        if (result){
          this.loading = true
          axios.post("/generate-video",{
            name: this.name
          })
          .then((response) =>{
            response = response.data

            if(response.success){
              this.url = this.utf8_to_b64(response.url)
              window.location = '/video/'+ this.url + '/' + this.name
            } else {
              this.loading = false
              if(response.badwords) {
                this.name = ''
                new Noty({
                  text: "Por favor ingresa un nombre válido",
                  timeout: 5000,
                  closeWith: [],
                  layout: 'top',
                  killer:true,
                  force: true,
                  type: 'error',
                  animation: {
                    open: null,
                    close: null
                  }
                }).show();  
              } else {
                new Noty({
                  text: "Ocurrió un error al crear el video, por favor inténtelo nuevamente.",
                  timeout: 5000,
                  closeWith: [],
                  layout: 'top',
                  killer:true,
                  force: true,
                  type: 'error',
                  animation: {
                    open: null,
                    close: null
                  }
                }).show();  
              }
              
            }
            
          })
        }
      });
    },
    utf8_to_b64: function(str){
      return window.btoa(unescape(encodeURIComponent( str )));
    },
    shareFacebook: function(){
      // FB.getLoginStatus(function(response) {
      //   if (response.status === 'connected') {
      //     this.postVideo(response.authResponse);
      //   }
      //   else {
      //     FB.login(function(response) {
      //       if (response.authResponse) {
      //         this.postVideo(response.authResponse);
      //       } else {
      //       }
      //     }.bind(this), { scope: 'public_profile, publish_actions' });
      //   }
      // }.bind(this));
      window.open("https://www.facebook.com/sharer/sharer.php?u=" + window.location.href)
    },
    postVideo: function(response){
      var notyWaiting = new Noty({
        text: "Por favor espera mientras publicamos tu video",
        timeout: false,
        closeWith: [],
        layout: 'topCenter',
        type: 'alert',
        animation: {
          open: null,
          close: null
        }
      }).show();

      FB.api(
          response.userID+"/videos",
          "POST",
          {
              "file_url": window.url,
              "title": "Te entendemos mamá",
              "description": "Envíale un video personalizado a mamá y hazle saber que tú también la entiendes. " +window.location.href,
              "link": "http://google.com"
          },
          function (response) {
            notyWaiting.close()
            if (response && !response.error) {
              new Noty({
                text: "Felicitaciones, tu video ha sido publicado, en breve lo verás en tu muro de Facebook",
                timeout: 5000,
                closeWith: [],
                layout: 'top',
                type: 'success',
                killer:true,
                force: true,
                animation: {
                  open: null,
                  close: null
                }
              }).show();
            } else {
              new Noty({
                text: "Hubo un error al publicar el video, por favor inténtalo nuevamente",
                timeout: 5000,
                closeWith: [],
                layout: 'top',
                killer:true,
                force: true,
                type: 'error',
                animation: {
                  open: null,
                  close: null
                }
              }).show();
            }
          }
      );

    },
    sendIndex: function(){
      window.location = '/'
    },
    sendEmail: function(){
      this.$validator.validateAll().then((result) => {
        if (result){
          this.loadingEmail = true
          axios.post("/send-email",{
            email: this.email,
            url: window.location.href,
            name: window.name
          })
          .then((response) => {
            this.loadingEmail = false
            $("#emailModal").modal("hide")
            new Noty({
              text: "Listo, el correo fue enviado exitosamente",
              timeout: 5000,
              closeWith: [],
              layout: 'top',
              killer:true,
              force: true,
              type: 'success',
              animation: {
                open: null,
                close: null
              }
            }).show();
          })
        }
      })
    }
  }
})