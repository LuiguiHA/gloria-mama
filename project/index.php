<?php
session_start();

require 'vendor/autoload.php';

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use Mailgun\Mailgun;

$app = new \Slim\App([
    'settings' => [
        'displayErrorDetails' => true,

        'logger' => [
            'name' => 'slim-app',
            'level' => Monolog\Logger::DEBUG,
            'path' => __DIR__ . 'logs/app.log',
        ],
    ],
]);


// Get container
$container = $app->getContainer();


// Register component on container
$container['view'] = function ($container) {
    // $view = new \Slim\Views\Twig('templates', [
    //     'cache' => 'cache'
    // ]);
    $view = new \Slim\Views\Twig('templates');

    // Instantiate and add Slim specific extension
    $basePath = rtrim(str_ireplace('index.php', '', $container['request']->getUri()->getBasePath()), '/');
    $view->addExtension(new Slim\Views\TwigExtension($container['router'], $basePath));
    return $view;
};

$app->get('/', function (Request $req,  Response $res, $args = []) {
    return $this->view->render($res, 'index.html', []);
});

$app->get('/aaa', function (Request $req,  Response $res, $args = []) {
    return $this->view->render($res, 'index.html', []);
});

$app->get('/video/{id}/{name}', function (Request $req,  Response $res, $args = []) {
    $url = $args['id'];
    $name = $args['name'];
    $video_url = base64_decode($url);
    return $this->view->render($res, 'video.html', ['url' => $video_url, 'name' => $name]);
});

$app->get('/mama/{name}', function (Request $req,  Response $res, $args = []) {
    $name = $args['name'];
    $video_url = "https://s3.us-east-2.amazonaws.com/videos-mamas/".$name.".mp4";
    return $this->view->render($res, 'video.html', ['url' => $video_url, 'name' => $name]);
});

$app->post('/send-email', function (Request $req,  Response $res, $args = []) {
    $params = $req->getParams();
    $email = $params['email'];
    $name = $params['name'];
    $url = $params['url'];
    $data = array('success' => true);
    $html = $this->view->render($res, 'email.html', ['name' => $name, 'url' => $url])->getBody();  
    $from = new SendGrid\Email('Te Entendemos Mamá', "hola@teentendemosmama.com");
    $subject = "Te entedemos mamá";
    $to = new SendGrid\Email(null, $email);
    $content = new SendGrid\Content("text/html", $html);
    $mail = new SendGrid\Mail($from, $subject, $to, $content);
    
    $apiKey = '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';
    $sg = new \SendGrid($apiKey);
    
    $response = $sg->client->mail()->send()->post($mail);
    return $res->withJson($data);
});

$app->post('/post-video', function (Request $req,  Response $res, $args = []) {

    $params = $req->getParams();

    $user_id = $params["user_id"];
    $access_token = $params["access_token"];
    $file_url = $params["file_url"];

    $url = "https://graph-video.facebook.com/v2.3/$user_id/videos";
    $data = array(
        "access_token" => $access_token, 
        "file_url" => $file_url
    );
    $response = Requests::post($url, array(), $data);
    $response_data = json_decode($response->body, true); 
    return $res->withJson($response_data);
});

$app->post('/generate-video', function (Request $req,  Response $res, $args = []) {

    $params = $req->getParams();
    $name = $params['name'];
    $data = array('success' => true, 'badwords' => false);

    $valid = true;

    foreach(file(getcwd()."/lista.txt") as $line) {
       
       $index = strpos(strtolower($line), strtolower($name));
       if ($index !== false) {
        $valid = false;
       }
    }

    if($valid){
        
        $url = 'https://render-us-west-2.impossible.io/v2/render/304849a5-7adf-44c1-bde0-a0b453f7d2fe';
        $headers = array('Content-Type' => 'application/json');
        $data = array(
            'movie' => 'gloria_correct', 
            "params" => array(
                "name" => $name
            ),
            "upload" => array(
                "extension" => "mp4",
                "async" => false,
                "destination" => array(
                    "type" => "s3",
                    "aws_use_policy" => true,
                    "filename" => uniqid()."-".date('Y-m-d-H:i:s')."$name.mp4",
                    "contenttype" => "video/mp4",
                    "bucket" => "gloria-mama-videos",
                    "acl" => "public-read"
                )
            )
        );
        $response = Requests::post($url, $headers, json_encode($data));
        $response_data = json_decode($response->body, true); 
        $url_extra = "teentendemosmama.com/video/".$response_data["upload"]["result"].$name;
        $data = array('url' => $response_data["upload"]["result"], 'success' => true, 'url_extra' => $url_extra);
        return $res->withJson($data);

    } else {
        $data["success"] = false;
        $data["badwords"] = true;
        return $res->withJson($data);
    }

});

$app->run();